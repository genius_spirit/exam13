const mongoose = require('mongoose');
const Schema = mongoose.Schema;

const ImageSchema = new Schema({
	userId: {
		type: Schema.Types.ObjectId,
		ref: 'User',
		required: true
	},
	placeId: {
		type: Schema.Types.ObjectId,
		ref: 'Place',
		required: true
	}
});

const RateSchema = new Schema({
	userId: {
		type: Schema.Types.ObjectId,
		ref: 'User',
		required: true
	},
	title: {
		type: String,
		required: true
	},
	rating: {
		type: Number,
		required: true
	}
});

const ReviewSchema = new Schema({
	userId: {
		type: Schema.Types.ObjectId,
		ref: 'User',
		required: true
	},
	text: {
		type: String,
		required: true
	},
	rate: [RateSchema]
});

const PlaceSchema = new Schema({
	title: {
		type: String,
		required: true
	},
	description: {
		type: String,
		required: true
	},
	mainImage: {
		type: String,
		required: true
	},
	userId: {
		type: Schema.Types.ObjectId,
		ref: 'User',
		required: true
	},
	images: [ImageSchema],
	reviews: [ReviewSchema]
});

const Place = mongoose.model('Place', PlaceSchema);
module.exports = Place;