const express = require('express');
const Place = require('../models/Place');
const multer = require('multer');
const path = require('path');
const nanoid = require('nanoid');
const auth = require('../middleware/auth');
const config = require('../config');

const storage = multer.diskStorage({
	destination: (req, file, cb) => {
		cb(null, config.uploadPath);
	},
	filename: (req, file, cb) => {
		cb(null, nanoid() + path.extname(file.originalname));
	},
});

const upload = multer({ storage });

const createRouter = () => {
	const router = express.Router();

	router.get('/:id', auth, async(req, res) => {
		const id = req.params.id;
		try {
			const result = await Place.findOne({_id: id});
			if (result) res.send(result);
			else res.status(404).send({message: `Place ${id} not found`})
		} catch (e) {
			return  res.status(400).send({error: e});
		}
	});

	router.post('/', [auth, upload.single('image')], async (req, res) => {
		const data = {
			title: req.body.title,
			description: req.body.description,
			userId: req.body.user,
			mainImage: req.file.filename,
		};

		try {
			const newPlace = new Place(data);
			const answer = await newPlace.save();
			res.send(answer);
		} catch (e) {
			return res.status(400).send({ error });
		}
	});

	router.post('/reviews', auth,  async (req, res) => {
		try {
			const review = await Place.findOneAndUpdate({_id: req.body.id}, {$push: {reviews: {userId: req.body.userId, text: req.body.review, rate: [{title: 'quality', rating: req.body.quality },
							{title: 'service', rating: req.body.service },
							{title: 'interior', rating: req.body.interior }]}}});
			if (review) res.send(review);
		} catch (e) {
			return res.status(400).send({error: 'can not update reviews'});
		}
	});

	router.get('/', async (req, res) => {
		try {
			const places = await Place.find().populate('userId');
			if (places.length > 0) res.send(places);
		} catch (e) {
			return res.status(400).send({error: e});
		}
	});



	return router;
};

module.exports = createRouter;
