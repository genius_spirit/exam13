import React, { Fragment } from "react";
import { NotificationContainer } from "react-notifications";
import "react-notifications/lib/notifications.css";
import Toolbar from "../../components/Toolbar/Toolbar";
import { connect } from "react-redux";
import { logoutUser } from "../../store/actions/users";

const Layout = props => (
	<Fragment>
		<NotificationContainer />
		<header>
			<Toolbar user={props.user} logout={props.logoutUser} />
		</header>
		<div style={{ maxWidth: "1280px", height: "auto", margin: "0 auto" }}>
			<main className="container">{props.children}</main>
		</div>
	</Fragment>
);

const mapStateToProps = state => {
	return {
		user: state.users.user,
	};
};

const mapDispatchToProps = dispatch => {
	return {
		logoutUser: () => dispatch(logoutUser()),
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Layout);
