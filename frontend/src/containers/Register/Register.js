import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { Alert, Button, Col, Form, FormGroup, PageHeader } from "react-bootstrap";
import { registerUser } from "../../store/actions/users";
import FormElement from "../../components/UI/Form/FormElement/FormElement";

class Register extends Component {
	state = {
		username: "",
		password: "",
	};

	inputChangeHandler = event => {
		this.setState({
			[event.target.name]: event.target.value,
		});
	};

	submitFormHandler = event => {
		event.preventDefault();
		this.props.onRegisterUser(this.state);
	};

	render() {
		return (
			<Fragment>
				<PageHeader>Register new user</PageHeader>
				<Form horizontal onSubmit={this.submitFormHandler}>
					{this.props.error && <Alert bsStyle="danger">{this.props.error.error}</Alert>}
					<FormElement
						propertyName="username"
						title="Username"
						type="text"
						value={this.state.username}
						changeHandler={this.inputChangeHandler}
						autoComplete="off"
					/>

					<FormElement
						propertyName="password"
						title="Password"
						type="password"
						value={this.state.password}
						changeHandler={this.inputChangeHandler}
						autoComplete="off"
					/>

					<FormGroup>
						<Col smOffset={2} sm={10}>
							<Button bsStyle="primary" type="submit">
								Register
							</Button>
						</Col>
					</FormGroup>
				</Form>
			</Fragment>
		);
	}
}

const mapStateToProps = state => {
	return {
		error: state.users.registerError,
	};
};

const mapDispatchToProps = dispatch => {
	return {
		onRegisterUser: userData => dispatch(registerUser(userData)),
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Register);
