import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { getInfoAboutPlace } from "../../store/actions/places";
import config from "../../config";
import FormElement from "../../components/UI/Form/FormElement/FormElement";
import { Button, Col, Form, FormGroup, PageHeader } from "react-bootstrap";
import { sendReviews } from "../../store/actions/reviews";

const styles = {
	placeWrapper: { display: "flex", flexWrap: "wrap", justifyContent: "space-between", marginBottom: "30px" },
	placeImage: { width: "400px", height: "auto" },
	placeTitle: { width: "700px", padding: "0 15px" },
	placeGallery: { padding: "0 15px", marginBottom: "30px" },
	placeReview: { padding: "0 15px", marginBottom: "30px" },
	placeNewImage: { padding: "0 15px", marginBottom: "50px" },
};
const QUALITY = [{ title: 1 }, { title: 2 }, { title: 3 }, { title: 4 }, { title: 5 }];

class Place extends Component {
	state = {
		review: "",
		quality: "",
		service: "",
		interior: "",
		image: "",
	};

	componentDidMount() {
		const id = this.props.match.params.id;
		this.props.onGetInfoAboutPlace(id);
	}

	inputChangeHandler = event => {
		this.setState({
			[event.target.name]: event.target.value,
		});
	};

	fileChangeHandler = event => {
		this.setState({
			[event.target.name]: event.target.files[0],
		});
	};

	submitReviewFormHandler = event => {
		event.preventDefault();
		const data = {
			id: this.props.match.params.id,
			review: this.state.review,
			quality: this.state.quality,
			service: this.state.service,
			interior: this.state.interior,
			userId: this.props.user._id
		};
		this.props.onSendReviews(data);
	};

	render() {
		const place = this.props.place;

		return (
			<Fragment>
				{place && (
					<Fragment>
						<div style={styles.placeWrapper}>
							<div style={styles.placeTitle}>
								<h1>{place.title}</h1>
								<p>{place.description}</p>
							</div>
							<div style={styles.placeImage}>
								<img src={config.apiUrl + "/uploads/" + place.mainImage} alt={place.title} width="100%" />
							</div>
						</div>

						<div style={styles.placeGallery}>
							<PageHeader>Gallery</PageHeader>
						</div>

						<div style={styles.placeReview}>
							<PageHeader>Reviews</PageHeader>

							<Form horizontal onSubmit={this.submitReviewFormHandler}>
								<FormElement
									propertyName="review"
									title="Add Review"
									type="textarea"
									value={this.state.review}
									changeHandler={this.inputChangeHandler}
									// required
								/>
								<FormElement
									propertyName="quality"
									title="Quality of food"
									type="select"
									options={QUALITY}
									value={this.state.quality}
									changeHandler={this.inputChangeHandler}
									required
								/>
								<FormElement
									propertyName="service"
									title="Service quality"
									type="select"
									options={QUALITY}
									value={this.state.service}
									changeHandler={this.inputChangeHandler}
									required
								/>
								<FormElement
									propertyName="interior"
									title="Interior"
									type="select"
									options={QUALITY}
									value={this.state.interior}
									changeHandler={this.inputChangeHandler}
									required
								/>
								<FormGroup>
									<Col smOffset={2} sm={10}>
										<Button type="submit">Submit review</Button>
									</Col>
								</FormGroup>
							</Form>
						</div>

						<div style={styles.placeNewImage}>
							<PageHeader>Upload new photo</PageHeader>

							<FormElement
								propertyName="image"
								title="Choose new photo"
								type="file"
								changeHandler={this.fileChangeHandler}
								required
							/>
						</div>

						<div>
							<FormGroup>
								<Col smOffset={2} sm={10}>
									<Button bsStyle="primary" type="submit">
										Upload
									</Button>
								</Col>
							</FormGroup>
						</div>
					</Fragment>
				)}
			</Fragment>
		);
	}
}

const mapStateToProps = state => {
	return {
		place: state.places.place,
		user: state.users.user
	};
};

const mapDispatchToProps = dispatch => {
	return {
		onGetInfoAboutPlace: id => dispatch(getInfoAboutPlace(id)),
		onSendReviews: data => dispatch(sendReviews(data)),
	};
};

export default connect(
	mapStateToProps,
	mapDispatchToProps
)(Place);
