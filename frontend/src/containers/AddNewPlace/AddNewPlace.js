import React, { Component, Fragment } from 'react';
import { PageHeader } from 'react-bootstrap';
import NewPlaceForm from '../../components/NewPlaceForm/NewPlaceForm';
import { connect } from 'react-redux';
import { addNewPlace } from '../../store/actions/places';

class AddNewPlace extends Component {

	newPlaceHandler = data => {
		this.props.onNewPlaceAdd(data);
	};

	render() {
		return (
			<Fragment>
				<PageHeader>Add new place</PageHeader>
				<NewPlaceForm onSubmit={this.newPlaceHandler} user={this.props.user} />
			</Fragment>
		);
	}
}

const mapStateToProps = state => {
  return {
    user: state.users.user
  }
};

const mapDispatchToProps = dispatch => {
  return {
    onNewPlaceAdd: data => dispatch(addNewPlace(data))
  }
};

export default connect(mapStateToProps, mapDispatchToProps)(AddNewPlace);