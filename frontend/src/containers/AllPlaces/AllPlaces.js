import React, { Component, Fragment } from "react";
import { connect } from "react-redux";
import { getAllPlaces } from "../../store/actions/places";
import { PageHeader } from "react-bootstrap";
import config from "../../config";

const styles = {
	wrapper: { display: "flex", flexWrap: "wrap", justifyContent: "space-around" },
	placeCard: {
		marginBottom: "50px",
		width: "200px",
		height: "auto",
		textAlign: "center",
		boxShadow: "0 4px 8px 0 rgba(0, 0, 0, 0.2), 0 6px 20px 0 rgba(0, 0, 0, 0.19)",
	},
	placeImg: { width: "100%", height: "auto" },
};

class AllPlaces extends Component {
	componentDidMount() {
		this.props.onGetAllPlaces();
	}

	render() {
		return (
			<Fragment>
				<PageHeader style={{ marginBottom: "50px" }}>All places</PageHeader>
				<div style={styles.wrapper}>
					{this.props.allPlaces &&
						this.props.allPlaces.map(item => (
							<div
								style={styles.placeCard}
								key={item._id}
								onClick={() => this.props.history.push("/places/" + item._id)}
							>
								<img
									src={config.apiUrl + "/uploads/" + item.mainImage}
									alt={item.title}
									style={styles.placeImg}
								/>
								<h4>{item.title}</h4>
							</div>
						))}
				</div>
			</Fragment>
		);
	}
}
const mapStateToProps = state => {
	return {
		allPlaces: state.places.allPlaces,
	};
};

const mapDispatchToProps = dispatch => {
	return {
		onGetAllPlaces: () => dispatch(getAllPlaces()),
	};
};
export default connect(
	mapStateToProps,
	mapDispatchToProps
)(AllPlaces);
