import React from "react";
import { Redirect, Route, Switch, withRouter } from 'react-router-dom';
import { connect } from "react-redux";

import Register from "./containers/Register/Register";
import Login from "./containers/Login/Login";
import AddNewPlace from './containers/AddNewPlace/AddNewPlace';
import AllPlaces from "./containers/AllPlaces/AllPlaces";
import Place from "./containers/Place/Place";

const ProtectedRoute = ({ isAllowed, ...props }) =>
	isAllowed ? <Route {...props} /> : <Redirect to="/login" />;

const Routes = ({user}) => {
  return (
    <Switch>
			<Route path="/" exact component={AllPlaces} />
      <Route path="/register" exact component={Register} />
      <Route path="/login" exact component={Login} />
			<ProtectedRoute
				isAllowed={user && (user.role === "admin" || user.role === "user")}
				path="/places/add-new"
				exact
				component={AddNewPlace}
			/>
			<ProtectedRoute
				isAllowed={user && (user.role === "admin" || user.role === "user")}
				path="/places/:id"
				exact
				component={Place}
			/>
      <Route
        render={() => <h1 style={{ textAlign: "center" }}>Page not found</h1>}
      />
    </Switch>
  );
};

const mapStateToProps = state => {
  return {
    user: state.users.user
  };
};

export default withRouter(connect(mapStateToProps)(Routes));
