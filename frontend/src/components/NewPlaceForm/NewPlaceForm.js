import React, { Component } from "react";
import FormElement from "../UI/Form/FormElement/FormElement";
import { Button, Checkbox, Col, Form, FormGroup } from "react-bootstrap";
import { NotificationManager } from "react-notifications";

class NewPlaceForm extends Component {
	state = {
		title: "",
		description: "",
		image: "",
		isChecked: false,
	};

	submitFormHandler = event => {
		event.preventDefault();
		if (this.state.isChecked) {
			const formData = new FormData();
			Object.keys(this.state).forEach(key => {
				formData.append(key, this.state[key]);
			});
			formData.append("user", this.props.user._id);
			this.props.onSubmit(formData);
		} else NotificationManager.error("Please check 'I understand' before add new place");
	};

	inputChangeHandler = event => {
		this.setState({
			[event.target.name]: event.target.value,
		});
	};

	fileChangeHandler = event => {
		this.setState({
			[event.target.name]: event.target.files[0],
		});
	};

	checkboxHandler = () => {
		this.setState({ isChecked: !this.state.isChecked });
	};

	render() {
		return (
			<Form horizontal onSubmit={this.submitFormHandler}>
				<FormElement
					propertyName="title"
					title="Title"
					type="text"
					value={this.state.title}
					changeHandler={this.inputChangeHandler}
					required
				/>

				<FormElement
					propertyName="description"
					title="Description"
					type="textarea"
					value={this.state.description}
					changeHandler={this.inputChangeHandler}
					required
				/>

				<FormElement
					propertyName="image"
					title="Main photo"
					type="file"
					changeHandler={this.fileChangeHandler}
					required
				/>

				<FormGroup style={{ marginTop: "30px" }}>
					<Col smOffset={2} sm={10}>
						<div style={{ padding: "0 20px 10px 0", fontSize: "13px" }}>
							Lorem ipsum dolor sit amet, consectetur adipisicing elit. Ab accusantium aliquid, aut beatae
							deserunt dolorum, ducimus ea harum impedit itaque maiores minima neque nobis non, odit quas quos
							sit unde!
						</div>
						<Checkbox onClick={this.checkboxHandler}>I understand</Checkbox>
					</Col>
				</FormGroup>

				<FormGroup>
					<Col smOffset={2} sm={10}>
						<Button bsStyle="primary" type="submit">
							Submit new place
						</Button>
					</Col>
				</FormGroup>
			</Form>
		);
	}
}

export default NewPlaceForm;
