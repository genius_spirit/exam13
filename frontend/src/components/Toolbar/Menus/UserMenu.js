import React, { Fragment } from "react";
import { MenuItem, Nav, NavDropdown, NavItem } from 'react-bootstrap';
import { LinkContainer } from "react-router-bootstrap";

const UserMenu = ({ user, logout }) => {

  const navTitle = (
    <Fragment>
      Hello, <b>{user.username}</b>
    </Fragment>
  );

  return (
    <Nav pullRight>
      <NavDropdown title={navTitle} id="user-menu">
        <MenuItem onClick={logout}>Logout</MenuItem>
      </NavDropdown>
				<LinkContainer to="/places/add-new"><NavItem >Add new place</NavItem></LinkContainer>
    </Nav>
  );
};

export default UserMenu;
