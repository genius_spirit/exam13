import {
	ADD_NEW_PLACE_FAILURE,
	ADD_NEW_PLACE_SUCCESS,
	GET_ALL_PLACES_FAILURE,
	GET_ALL_PLACES_SUCCESS, GET_INFO_ABOUT_PLACE_FAILURE, GET_INFO_ABOUT_PLACE_SUCCESS,
} from "../actions/actionTypes";

const initialState = {
	newPlace: null,
	error: null,
	allPlaces: [],
	place: []
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
		case ADD_NEW_PLACE_SUCCESS:
			return {...state, newPlace: action.data, error: null};
		case ADD_NEW_PLACE_FAILURE:
			return {...state, error: action.error};
		case GET_ALL_PLACES_SUCCESS:
			return {...state, allPlaces: action.data, error: null};
		case GET_ALL_PLACES_FAILURE:
			return {...state, error: action.error};
		case GET_INFO_ABOUT_PLACE_SUCCESS:
			return {...state, place: action.data, error: null};
		case GET_INFO_ABOUT_PLACE_FAILURE:
			return {...state, error: action.error};
    default:
      return state;
  }
};

export default reducer;