import { SEND_REVIEWS_FAILURE, SEND_REVIEWS_SUCCESS } from "../actions/actionTypes";

const initialState = {
	reviews: [],
	error: null
};

const reducer = (state = initialState, action) => {
  switch (action.type) {
		case	SEND_REVIEWS_SUCCESS:
			return {...state, reviews: action.data, error: null};
		case SEND_REVIEWS_FAILURE:
			return {...state, error: action.error};
    default:
      return state;
  }
};

export default reducer;