import axios from "../../axios-api";
import {put} from 'redux-saga/effects';
import { push } from "react-router-redux";
import {
	addNewPlaceFailure,
	addNewPlaceSuccess,
	getAllPlacesFailure,
	getAllPlacesSuccess, getInfoAboutPlaceFailure,
	getInfoAboutPlaceSuccess,
} from "../actions/places";
import { NotificationManager } from 'react-notifications';

export function* addNewPlaceSaga(action) {
	try {
		const response = yield axios.post("/places", action.data);
		if (response) {
			yield put(addNewPlaceSuccess(response.data));
			yield put(push("/"));
			NotificationManager.success(response.data.title + " added successfully");
		}
	} catch (e) {
		yield put(addNewPlaceFailure(e.response.data));
	}
}

export function* getAllPlacesSaga() {
	try {
		const response = yield axios.get("/places");
		if (response) {
			yield put(getAllPlacesSuccess(response.data));
		}
	} catch (e) {
		yield put(getAllPlacesFailure(e.response.data));
	}
}

export function* getInfoAboutPlaceSaga(action) {
	try {
		const response = yield axios.get("/places/" + action.id);
		if (response) {
			yield put(getInfoAboutPlaceSuccess(response.data));
		}
	} catch (e) {
		yield put(getInfoAboutPlaceFailure(e.response.data));
	}
}