import axios from "../../axios-api";
import {put} from 'redux-saga/effects';
import { sendReviewsFailure, sendReviewsSuccess } from "../actions/reviews";

export function* sendReviewsSaga(action) {
	try {
		const response = yield axios.post("/places/reviews", action.data);
		if (response) {
			console.log(":________", response.data);
			yield put(sendReviewsSuccess(response.data));
		}
	} catch (e) {
		yield put(sendReviewsFailure(e.response.data));
	}
}