import {takeEvery, all} from 'redux-saga/effects';
import {
	ADD_NEW_PLACE,
	GET_ALL_PLACES, GET_INFO_ABOUT_PLACE,
	LOGIN_USER,
	LOGOUT_USER,
	LOGOUT_USER_EXPIRED,
	REGISTER_USER, SEND_REVIEWS,
} from "../actions/actionTypes";
import {loginUserSaga, logoutExpiredUserSaga, logoutUserSaga, registerUserSaga } from "./users";
import { addNewPlaceSaga, getAllPlacesSaga, getInfoAboutPlaceSaga } from "./places";
import { sendReviewsSaga } from "./reviews";


export function * registerUser() {
  yield takeEvery(REGISTER_USER, registerUserSaga);
}

function* logoutUser() {
  yield takeEvery(LOGOUT_USER, logoutUserSaga);
}

function* loginUser() {
  yield takeEvery(LOGIN_USER, loginUserSaga);
}

function* logoutExpiredUser() {
  yield takeEvery(LOGOUT_USER_EXPIRED, logoutExpiredUserSaga);
}

function* addNewPlace() {
	yield takeEvery(ADD_NEW_PLACE, addNewPlaceSaga);
}

function* getAllPlaces() {
	yield takeEvery(GET_ALL_PLACES, getAllPlacesSaga);
}

function* getInfoAboutPlace() {
	yield takeEvery(GET_INFO_ABOUT_PLACE, getInfoAboutPlaceSaga);
}

function* sendReviews() {
	yield takeEvery(SEND_REVIEWS, sendReviewsSaga);
}

export function* rootSaga() {
	yield all([
		registerUser(),
		logoutUser(),
		loginUser(),
		logoutExpiredUser(),
		addNewPlace(),
		getAllPlaces(),
		getInfoAboutPlace(),
		sendReviews(),
	]);
}