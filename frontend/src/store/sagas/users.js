import axios from "../../axios-api";
import {put} from 'redux-saga/effects';
import {loginUserFailure, loginUserSuccess, logoutExpiredUserSuccess,
  logoutUserSuccess,
  registerUserFailure,
  registerUserSuccess
} from "../actions/users";
import { push } from "react-router-redux";
import { NotificationManager } from "react-notifications";

export function* registerUserSaga(action) {
  try {
    yield axios.post("/users", action.userData);
    yield put(registerUserSuccess());
    yield put(push("/login"));
    yield NotificationManager.success('Success', "Register successful");
  } catch (error) {
    yield put(registerUserFailure(error.response.data));
  }
}

export function* loginUserSaga(action) {
  try {
    const response = yield axios.post("/users/sessions", action.userData);
    if (response) {
      yield put(loginUserSuccess(response.data.user, response.data.token));
      yield put(push("/"));
      yield NotificationManager.success('Success', "Login successful");
    }
  } catch (error) {
    const errorObj = error.response
      ? error.response.data
      : { error: "No internet" };
    yield put(loginUserFailure(errorObj));
  }
}

export function* logoutUserSaga() {
  try {
    yield axios.delete('/users/sessions');
    yield put(logoutUserSuccess());
    yield put(push("/"));
    NotificationManager.success('Success', "Successfully LogOut");
  } catch (e) {
    NotificationManager.error('Error', "Logout failed");
  }
}

export function* logoutExpiredUserSaga() {
  try {
    yield put(logoutExpiredUserSuccess());
    yield put(push("/"));
    NotificationManager.error('Error', 'Expired time of session');
  } catch (error) {
    console.log("logout expired user error:________", error);
  }
}