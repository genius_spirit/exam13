import {
	ADD_NEW_PLACE,
	ADD_NEW_PLACE_FAILURE,
	ADD_NEW_PLACE_SUCCESS,
	GET_ALL_PLACES, GET_ALL_PLACES_FAILURE,
	GET_ALL_PLACES_SUCCESS, GET_INFO_ABOUT_PLACE, GET_INFO_ABOUT_PLACE_FAILURE, GET_INFO_ABOUT_PLACE_SUCCESS,
} from "./actionTypes";


export const addNewPlaceSuccess = data => {
	return {type: ADD_NEW_PLACE_SUCCESS, data};
};

export const addNewPlaceFailure = error => {
	return {type: ADD_NEW_PLACE_FAILURE, error};
};

export const addNewPlace = data => {
	return {type: ADD_NEW_PLACE, data};
};

export const getAllPlacesSuccess = data => {
	return {type: GET_ALL_PLACES_SUCCESS, data};
};

export const getAllPlacesFailure = error => {
	return {type: GET_ALL_PLACES_FAILURE, error};
};

export const getAllPlaces = () => {
	return {type: GET_ALL_PLACES};
};

export const getInfoAboutPlaceSuccess = data => {
	return {type: GET_INFO_ABOUT_PLACE_SUCCESS, data};
};

export const getInfoAboutPlaceFailure = error => {
	return {type: GET_INFO_ABOUT_PLACE_FAILURE, error};
};

export const getInfoAboutPlace = id => {
	return {type: GET_INFO_ABOUT_PLACE, id};
};