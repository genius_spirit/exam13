import {
	LOGIN_USER,
	LOGIN_USER_FAILURE,
	LOGIN_USER_SUCCESS,
	LOGOUT_USER,
	LOGOUT_USER_EXPIRED,
	LOGOUT_USER_SUCCESS,
	REGISTER_USER,
	REGISTER_USER_FAILURE,
	REGISTER_USER_SUCCESS,
} from "./actionTypes";

export const registerUserSuccess = () => {
	return { type: REGISTER_USER_SUCCESS };
};

export const registerUserFailure = error => {
	return { type: REGISTER_USER_FAILURE, error };
};

export const registerUser = userData => {
	return { type: REGISTER_USER, userData };
};

export const loginUserSuccess = (user, token) => {
	return { type: LOGIN_USER_SUCCESS, user, token };
};

export const loginUserFailure = error => {
	return { type: LOGIN_USER_FAILURE, error };
};

export const loginUser = userData => {
	return { type: LOGIN_USER, userData };
};

export const logoutUser = () => {
	return { type: LOGOUT_USER };
};

export const logoutUserSuccess = () => {
	return { type: LOGOUT_USER_SUCCESS };
};

export const logoutExpiredUser = () => {
	return { type: LOGOUT_USER_EXPIRED };
};

export const logoutExpiredUserSuccess = () => {
	return { type: LOGOUT_USER_SUCCESS };
};
