import { SEND_REVIEWS, SEND_REVIEWS_FAILURE, SEND_REVIEWS_SUCCESS } from "./actionTypes";

export const sendReviews = data => {
	return {type: SEND_REVIEWS, data};
};

export const sendReviewsSuccess = data => {
	return {type: SEND_REVIEWS_SUCCESS, data};
};

export const sendReviewsFailure = error => {
	return {type: SEND_REVIEWS_FAILURE, error};
}