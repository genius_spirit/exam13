#!/bin/bash

echo "### Current dir is: $PWD"

cd ../frontend
echo "### Installation dependencies"
yarn

echo "### Start frontend"
pm2 start "yarn start" --name frontend

cd ../api
echo "### Installation dependencies"
yarn

echo "### Start backend"
pm2 start "yarn dev" --name api
